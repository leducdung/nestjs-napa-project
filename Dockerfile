FROM node:17-alpine AS local

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install glob rimraf

RUN npm install --only=local

COPY . .

RUN npm run build

FROM node:17-alpine as development

ARG NODE_ENV=development
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --only=development

COPY . .

COPY --from=local /usr/src/app/dist ./dist

CMD ["node", "dist/main"]