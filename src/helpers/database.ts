import { Connection, Schema } from 'mongoose';
import { DatabaseProviderName } from '../common/constants';

interface BuildSchemaProvider {
  name: string;
  schema: Schema;
}

export const buildSchemaProvider = ({ name, schema }: BuildSchemaProvider) => ({
  provide: name,
  useFactory: (connection: Connection) => connection.model(name, schema),
  inject: [DatabaseProviderName],
});
