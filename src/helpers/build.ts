export const buildFindingQuery = ({ query, fieldNeedToUseRegex = [] }) => {
  const { sortBy = '_id', limit, sortDirection, ...findingQuery } = query;
  const validDirection: number = sortDirection === 'ASC' ? 1 : -1;
  const hasPage = !!limit;
  const sortingCondition = { [sortBy]: validDirection };

  for (const key in findingQuery) {
    if (!key) {
      continue;
    }

    if (fieldNeedToUseRegex.includes(key)) {
      findingQuery[key] = { $regex: findingQuery[key], $options: 'i' };

      continue;
    }

    if (!Array.isArray(findingQuery[key])) {
      continue;
    }

    findingQuery[key] = { $in: findingQuery[key] };
  }

  const findAllQuery = { ...findingQuery };

  return {
    sortingCondition,
    findingQuery,
    findAllQuery,
    hasPage,
  };
};

export const buildOTPEmailData = ({ to, subject, text, typeEmail, otp }) => {
  const msg = {
    to,
    from: 'leducdung1409@gmail.com',
    subject,
    text,
    html: `<p>Your ${typeEmail} otp: <strong>${otp}</strong></p>`,
  };
  return msg;
};
