import * as mongoose from 'mongoose';
import { UserStatus } from '../../../common/enum';

export const UsersSchema = new mongoose.Schema(
  {
    username: {
      type: String,
    },
    status: {
      type: String,
      enum: [UserStatus.INACTIVE, UserStatus.ACTIVE],
      default: UserStatus.INACTIVE,
    },
    profilePhoto: { type: String },
    email: { type: String, unique: true, sparse: true },
    password: { type: String, select: false },
    firstName: { type: String, default: null },
    lastName: { type: String, default: null },
    fullName: { type: String, default: null },
    googleId: { type: String, unique: true, sparse: true, default: null },
    githubId: { type: String, unique: true, sparse: true, default: null },
  },
  {
    versionKey: false,
    timestamps: true,
  },
);
