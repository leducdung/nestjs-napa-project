import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsString,
  IsEmail,
  IsOptional,
  Length,
  IsNotEmpty,
  MinLength,
  MaxLength,
  Matches,
  IsMongoId,
  IsEnum,
  IsNumber,
  IsPositive,
  Min,
} from 'class-validator';
import { Types } from 'mongoose';
import { SortDirection, UserStatus } from '../../../common/enum';
import { SortBy } from './users.interface';

export class UserSingUpDto {
  @ApiProperty({
    description: 'Unique email address',
    required: true,
    example: 'sample@sample.com',
  })
  @IsNotEmpty()
  @Length(1, 320)
  @IsEmail()
  email: string;

  @ApiProperty({
    description: 'User Name',
    required: true,
    example: 'leducdung123',
  })
  @IsNotEmpty()
  @IsString()
  @Length(1, 50)
  username: string;

  @ApiProperty({
    description: 'password',
    required: true,
    example: 'yourSecretPassword',
  })
  @IsNotEmpty()
  @IsString()
  @MinLength(8, {
    message: 'Password is too short. Need at least 8 charter.',
  })
  @MaxLength(20, {
    message: 'Password is too Large. Not more then 20 charter.',
  })
  @Matches(/(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*\+,-.\/\\:;<=>?@[\]^_`{|}~])/, {
    message:
      'Password must have at least 1 number, 1 uppercase and 1 special characters',
  })
  password: string;

  @ApiProperty({
    description: 'User first name',
    required: true,
    example: 'First name',
  })
  @IsNotEmpty()
  @IsString()
  @Length(1, 50)
  firstName: string;

  @ApiProperty({
    description: 'User last name',
    required: true,
    example: 'Last name',
  })
  @IsNotEmpty()
  @IsString()
  @Length(1, 50)
  lastName: string;

  @ApiPropertyOptional({
    description: 'User Profile pic url',
    example: '/user/profilePic',
  })
  @IsOptional()
  @Length(0, 255)
  @IsString()
  profilePhoto?: string;
}

export class UserLoginDto {
  @ApiProperty({
    description: 'User Name',
    required: true,
    example: 'leducdung123',
  })
  @IsNotEmpty()
  @IsString()
  @Length(1, 50)
  username: string;

  @ApiProperty({
    description: 'password',
    required: true,
    example: 'yourSecretPassword',
  })
  @IsNotEmpty()
  @IsString()
  @MinLength(8, {
    message: 'Password is too short. Need at least 8 charter.',
  })
  @MaxLength(20, {
    message: 'Password is too Large. Not more then 20 charter.',
  })
  @Matches(/(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*\+,-.\/\\:;<=>?@[\]^_`{|}~])/, {
    message:
      'Password must have at least 1 number, 1 uppercase and 1 special characters',
  })
  password: string;
}

export class ChangePasswordDto {
  @ApiProperty({
    description: 'password',
    required: true,
    example: 'yourSecretPassword',
  })
  @IsNotEmpty()
  @IsString()
  @MinLength(8, {
    message: 'Password is too short. Need at least 8 charter.',
  })
  @MaxLength(20, {
    message: 'Password is too Large. Not more then 20 charter.',
  })
  @Matches(/(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*\+,-.\/\\:;<=>?@[\]^_`{|}~])/, {
    message:
      'Password must have at least 1 number, 1 uppercase and 1 special characters',
  })
  oldPassword: string;

  @ApiProperty({
    description: 'password',
    required: true,
    example: 'yourSecretPassword',
  })
  @IsNotEmpty()
  @IsString()
  @MinLength(8, {
    message: 'Password is too short. Need at least 8 charter.',
  })
  @MaxLength(20, {
    message: 'Password is too Large. Not more then 20 charter.',
  })
  @Matches(/(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*\+,-.\/\\:;<=>?@[\]^_`{|}~])/, {
    message:
      'Password must have at least 1 number, 1 uppercase and 1 special characters',
  })
  newPassword: string;
}

export class ResetPasswordDto {
  @ApiProperty({
    description: 'Unique email address',
    required: true,
    example: 'sample@sample.com',
  })
  @IsNotEmpty()
  @Length(1, 320)
  @IsEmail()
  email: string;

  @ApiPropertyOptional({
    description: 'one time password send your email',
    example: 'ag12sg2',
  })
  @IsOptional()
  @Length(0, 255)
  @IsString()
  otp?: string;

  @ApiProperty({
    description: 'password',
    required: true,
    example: 'yourSecretPassword',
  })
  @IsNotEmpty()
  @IsString()
  @MinLength(8, {
    message: 'Password is too short. Need at least 8 charter.',
  })
  @MaxLength(20, {
    message: 'Password is too Large. Not more then 20 charter.',
  })
  @Matches(/(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*\+,-.\/\\:;<=>?@[\]^_`{|}~])/, {
    message:
      'Password must have at least 1 number, 1 uppercase and 1 special characters',
  })
  newPassword: string;
}

export class UpdateUserDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  profilePhoto?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  fullName?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  firstName?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  lastName?: string;
}

export class ParamFindUser {
  @IsMongoId()
  @IsNotEmpty()
  readonly userId: Types.ObjectId;
}

const sortBy = [SortBy.id, SortBy.createdAt, SortBy.updatedAt];

export class FindManyUserDto {
  @ApiPropertyOptional({ enum: sortBy })
  @IsEnum(sortBy)
  @IsOptional()
  readonly sortBy?: SortBy = SortBy.id;

  @ApiPropertyOptional({ enum: SortDirection })
  @IsEnum(SortDirection)
  @IsOptional()
  readonly sortDirection?: SortDirection = SortDirection.asc;

  @ApiPropertyOptional()
  @IsNumber()
  @IsOptional()
  @IsPositive()
  @Type(() => Number)
  readonly limit?: number;

  @ApiPropertyOptional()
  @IsNumber()
  @IsOptional()
  @Min(0)
  @Type(() => Number)
  readonly offset?: number;

  @ApiPropertyOptional({ enum: UserStatus })
  @IsEnum(UserStatus)
  @IsOptional()
  status?: UserStatus;

  @ApiPropertyOptional({
    description: 'Unique email address',
    required: true,
    example: 'sample@sample.com',
  })
  @IsOptional()
  @IsEmail()
  email?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  fullName?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  firstName?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  lastName?: string;
}

export class ActiveOrInactiveAccountDto {
  @ApiPropertyOptional({ enum: UserStatus })
  @IsEnum(UserStatus)
  @IsOptional()
  status?: UserStatus;
}
