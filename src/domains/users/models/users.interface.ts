import { ICredential } from './../../../shared/validateAccess';
import { UserStatus } from '../../../common/enum';
import { Types } from 'mongoose';

export interface Users {
  id: string;
  username?: string;
  status: UserStatus;
  profilePhoto?: string;
  email?: string;
  password?: string;
  fullName?: string;
  firstName?: string;
  lastName?: string;
  googleId?: string;
  githubId?: string;
}

export interface ICreateUserData {
  username: string;
  status?: UserStatus;
  profilePhoto?: string;
  email: string;
  password: string;
  fullName?: string;
  firstName?: string;
  lastName?: string;
}

export interface ILoginData {
  username: string;
  password: string;
}

export interface ILoginService {
  loginData: ILoginData;
}

export interface ICreateUserService {
  createData: ICreateUserData;
}

export interface FindOneQuery {
  _id?: Types.ObjectId;
  email?: string;
}

export interface FindOneService {
  query: FindOneQuery;
  needToCheckExist?: boolean;
  needToValidateAccess?: boolean;
  credentials: ICredential;
}

export interface GetMeData {
  userId: string;
}

export interface GetMeService {
  query: GetMeData;
  credentials: ICredential;
}

export interface ChangePassWordData {
  oldPassword: string;
  newPassword: string;
}

export interface ChangePassWordService {
  data: ChangePassWordData;
  credentials: ICredential;
}

export interface ResetPassWordData {
  email: string;
  otp?: string;
  newPassword: string;
}

export interface ResetPassWordService {
  data: ResetPassWordData;
}

export enum SortBy {
  id = '_id',
  createdAt = 'createdAt',
  updatedAt = 'updatedAt',
}

export interface FindManyQuery {
  username?: string;
  status?: UserStatus;
  email?: string;
  fullName?: string;
  firstName?: string;
  lastName?: string;
  googleId?: string;
  githubId?: string;
  createdAt?: Date;
  updatedAt?: Date;
  sortBy?: string;
  offset?: number;
  cursor?: string;
  limit?: number;
  sortDirection?: string;
}

export interface FindManyUsersResult {
  totalCount: number;
  list: Users[];
}

export interface FindManyUsersService {
  query: FindManyQuery;
  credentials: ICredential;
}

export interface FindOneQuery {
  _id?: Types.ObjectId;
  userId?: Types.ObjectId | string;
  username?: string;
  email?: string;
  googleId?: string;
  githubId?: string;
}

export interface FindOneUsersService {
  query: FindOneQuery;
  credentials: ICredential;
}

export interface UpdateOneUsersData {
  status?: UserStatus;
  profilePhoto?: string;
  fullName?: string;
  firstName?: string;
  lastName?: string;
}

export interface UpdateOneUsersService {
  data: UpdateOneUsersData;
  credentials: ICredential;
  query: FindOneQuery;
}

export interface DeleteOneUsersService {
  query: FindOneQuery;
  credentials: ICredential;
}
