import { JwtAuthGuard } from './../../middlewares/auth/jwt-auth.guard';
import { AuthGuard } from '@nestjs/passport';
import {
  Controller,
  Post,
  Body,
  Get,
  UseGuards,
  Req,
  Request,
  Res,
  Put,
  Query,
  Param,
  Delete,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { checkControllerErrors } from '../../shared/checkErrors';
import {
  UserLoginDto,
  UserSingUpDto,
  ChangePasswordDto,
  ResetPasswordDto,
  FindManyUserDto,
  ParamFindUser,
  UpdateUserDto,
  ActiveOrInactiveAccountDto,
} from './models/users.dto';
import { FindManyUsersResult, Users } from './models/users.interface';
import { UsersService } from './users.service';
import { ConfigService } from '../../configs/configs.service';
import { Scopes } from '../../middlewares/authen/authen';
import { roleName } from '../../common/enum';

@ApiTags('Users')
@Controller()
export class UserController {
  constructor(
    private readonly userService: UsersService,
    private readonly configService: ConfigService,
  ) {}

  @Post('/auth/signup')
  @ApiOperation({ description: 'To register a new user account' })
  async registerUser(@Body() userSingUpDto: UserSingUpDto): Promise<Users> {
    try {
      const newUser = await this.userService.createOne({
        createData: userSingUpDto,
      });

      return newUser;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Post('/auth/login')
  @ApiOperation({ description: 'To login your account' })
  async loginUser(
    @Body() userLoginDto: UserLoginDto,
  ): Promise<{ token: string }> {
    try {
      const token = await this.userService.login({
        loginData: userLoginDto,
      });

      return token;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Get('auth/google')
  @UseGuards(AuthGuard('google'))
  async googleAuth(@Request() { user }) {
    //
  }

  @Get('auth/google/callback')
  @UseGuards(AuthGuard('google'))
  async googleAuthRedirect(@Request() { user }, @Res() res) {
    try {
      const googleLogin = await this.userService.googleLogin(user);

      return res.redirect(
        `${this.configService.feHost}?token=${googleLogin.token}`,
      );
    } catch (error) {
      if (error?.code && error.name) {
        return res.redirect(
          `${this.configService.feHost}/error?code=${error.code}&name=${error.name}`,
        );
      }

      checkControllerErrors(error);
    }
  }

  @Get('auth/github')
  @UseGuards(AuthGuard('github'))
  async githubAuth() {
    //
  }

  @Get('auth/github/callback')
  @UseGuards(AuthGuard('github'))
  async githubAuthCallback(@Req() req, @Res() res) {
    try {
      const githubLogin = await this.userService.githubLogin(req.user);

      return res.redirect(
        `${this.configService.feHost}?token=${githubLogin.token}`,
      );
    } catch (error) {
      if (error?.code && error.name) {
        return res.redirect(
          `${this.configService.feHost}/error?code=${error.code}&name=${error.name}`,
        );
      }

      checkControllerErrors(error);
    }
  }

  @Get('users/me')
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ description: 'To login your account' })
  async getMe(@Request() { user }) {
    try {
      const data = await this.userService.getMe({
        query: {
          userId: user.userId,
        },
        credentials: user,
      });

      return data;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Put('users/change/password')
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ description: 'To change password your account' })
  async changePassword(
    @Body() changePasswordDto: ChangePasswordDto,
    @Request() { user },
  ) {
    try {
      const isChange = await this.userService.changePassWord({
        data: changePasswordDto,
        credentials: user,
      });

      return {
        isChange,
      };
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Put('users/reset/password')
  @ApiOperation({ description: 'To reset password' })
  async resetPassword(@Body() resetPasswordDto: ResetPasswordDto) {
    try {
      const isChange = await this.userService.resetPassWord({
        data: resetPasswordDto,
      });

      return {
        isChange,
      };
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Get('/users')
  @UseGuards(JwtAuthGuard, new Scopes([roleName.ADMIN, roleName.BASIC]))
  @ApiOperation({ description: 'To Get many user' })
  async findMany(
    @Query() query: FindManyUserDto,
    @Request() { user },
  ): Promise<FindManyUsersResult> {
    try {
      const users = await this.userService.findMany({
        query,
        credentials: user,
      });

      return users;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Get('users/:userId')
  @UseGuards(JwtAuthGuard, new Scopes([roleName.ADMIN, roleName.BASIC]))
  @ApiOperation({ description: 'To Get one user' })
  async findOne(
    @Param() { userId }: ParamFindUser,
    @Request() { user },
  ): Promise<Users> {
    try {
      const findOne = await this.userService.findOne({
        query: {
          _id: userId,
        },
        credentials: user,
      });

      return findOne;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Put('users/:userId')
  @UseGuards(JwtAuthGuard, new Scopes([roleName.ADMIN, roleName.BASIC]))
  @ApiOperation({ description: 'To update user' })
  async updateOne(
    @Param() { userId }: ParamFindUser,
    @Body() updateUserDto: UpdateUserDto,
    @Request() { user },
  ): Promise<Users> {
    try {
      const updatedData = await this.userService.updateOne({
        data: updateUserDto,
        query: { _id: userId },
        credentials: user,
      });

      return updatedData;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Delete('admin/delete-user/:userId')
  @UseGuards(JwtAuthGuard, new Scopes([roleName.ADMIN]))
  @ApiOperation({ description: 'To delete user' })
  async deleteOne(
    @Param() { userId }: ParamFindUser,
    @Request() { user },
  ): Promise<{ isDeleted: boolean }> {
    try {
      const isDeleted = await this.userService.deleteOne({
        query: { _id: userId },
        credentials: user,
      });

      return {
        isDeleted,
      };
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Put('admin/active/inactive/:userId')
  @UseGuards(JwtAuthGuard, new Scopes([roleName.ADMIN]))
  @ApiOperation({ description: 'To active/inactive account' })
  async activeOrInactiveAccount(
    @Param() { userId }: ParamFindUser,
    @Body() activeOrInactiveAccountDto: ActiveOrInactiveAccountDto,
    @Request() { user },
  ): Promise<boolean> {
    try {
      const userAccess = await this.userService.updateOne({
        data: activeOrInactiveAccountDto,
        query: { _id: userId },
        credentials: user,
      });

      return userAccess;
    } catch (error) {
      checkControllerErrors(error);
    }
  }
}
