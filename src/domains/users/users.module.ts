import { GithubOauthStrategy } from './../../middlewares/strategies/github.strategy';
import { Module } from '@nestjs/common';
import { UserController } from './users.controller';
import { UsersService } from './users.service';
import { ConfigModule } from '../../configs/configs.module';
import { UsersSchema } from './models/users.schema';
import { DatabaseModule } from '../../database/database.module';
import { buildSchemaProvider } from '../../helpers/database';
import { AuthModule } from '../../middlewares/auth/auth.module';
import { forwardRef } from '@nestjs/common';
import { UserAccessModule } from '../userAccesses/userAccesses.module';
import { GoogleStrategy } from '../../middlewares/strategies/google.strategy';
import { OtpModule } from '../otps/otps.module';

const UsersProvider = buildSchemaProvider({
  name: 'Users',
  schema: UsersSchema,
});

@Module({
  imports: [
    AuthModule,
    DatabaseModule,
    ConfigModule,
    forwardRef(() => UserAccessModule),
    forwardRef(() => OtpModule),
  ],
  controllers: [UserController],
  providers: [UsersService, GoogleStrategy, GithubOauthStrategy, UsersProvider],
  exports: [UsersService],
})
export class UserModule {}
