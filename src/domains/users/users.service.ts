import {
  validateAccessToList,
  validateAccessToSingle,
} from './../../shared/validateAccess';
import {
  ChangePassWordService,
  DeleteOneUsersService,
  FindManyUsersResult,
  FindManyUsersService,
  FindOneService,
  GetMeService,
  ICreateUserService,
  ILoginService,
  ResetPassWordService,
  UpdateOneUsersService,
  Users,
} from './models/users.interface';
import * as bcrypt from 'bcryptjs';
import { ConfigService } from '../../configs/configs.service';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { roleName, UserStatus } from '../../common/enum';
import { AuthService } from '../../middlewares/auth/auth.service';
import { UserAccessesService } from '../userAccesses/userAccesses.service';
import { OtpsService } from '../otps/otps.service';
import { buildFindingQuery } from '../../helpers/build';

export type User = any;
@Injectable()
export class UsersService {
  constructor(
    @Inject('Users')
    private readonly usersModel: Model<Users>,
    private readonly configService: ConfigService,
    private readonly authService: AuthService,
    private readonly userAccessesService: UserAccessesService,
    private readonly otpsService: OtpsService,
  ) {}

  async createOne({ createData }: ICreateUserService): Promise<Users> {
    try {
      const isExitsUser = await this.usersModel
        .findOne({
          $or: [{ username: createData.username }, { email: createData.email }],
        })
        .exec();

      if (isExitsUser) {
        return Promise.reject({
          name: 'EmailOrUserNameExists',
          code: 400,
        });
      }

      const hashPass = await bcrypt.hash(
        createData.password,
        this.configService.bcryptSalt,
      );

      createData = {
        ...createData,
        status: UserStatus.ACTIVE,
        password: hashPass,
      };
      const createdUser = new this.usersModel(createData);
      const newUser = await createdUser.save();
      await this.userAccessesService.createOne({
        data: {
          userId: newUser._id,
          roleName: roleName.BASIC,
          status: UserStatus.ACTIVE,
        },
        credentials: {
          isAdmin: true,
        },
      });

      const newUserResult = newUser.toObject();
      delete newUserResult.password;

      return newUserResult;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async login({ loginData }: ILoginService): Promise<{ token: string }> {
    try {
      const user = await this.usersModel
        .findOne({ username: loginData.username })
        .select('username')
        .select('password')
        .select('status')
        .exec();

      if (!user) {
        return Promise.reject({
          name: 'UserNameNotFound',
          code: 404,
        });
      }

      if (user.status === UserStatus.INACTIVE) {
        return Promise.reject({
          name: 'InactiveUser',
          code: 403,
        });
      }
      const comparePassword = bcrypt.compareSync(
        loginData.password,
        user.password,
      );

      if (!comparePassword) {
        return Promise.reject({
          name: 'PasswordIsIncorrect',
          code: 400,
        });
      }

      const token = this.authService.createUserToken({ userId: user._id });

      return token;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getOne() {
    try {
      return 'ok';
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findOne({
    query,
    needToCheckExist = true,
    needToValidateAccess = true,
    credentials,
  }: FindOneService): Promise<User | undefined> {
    try {
      const user = await this.usersModel.findOne(query).exec();

      if (!user && needToCheckExist) {
        return Promise.reject({
          name: 'UserNotFound',
          code: 404,
        });
      }

      if (needToValidateAccess) {
        validateAccessToSingle({
          data: user,
          credentials,
        });
      }

      return user;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async googleLogin(data: any): Promise<{ token: string }> {
    try {
      const user = await this.usersModel
        .findOne({
          googleId: data.id,
        })
        .exec();

      let token;
      if (user?.status === UserStatus.INACTIVE) {
        return Promise.reject({
          name: 'InactiveUser',
          code: 403,
        });
      }

      if (user?.status === UserStatus.ACTIVE) {
        token = this.authService.createUserToken({ userId: user._id });

        return token;
      }

      const createData = {
        status: UserStatus.ACTIVE,
        email: data.email,
        firstName: data.firstName,
        lastName: data.lastName,
        profilePhoto: data.picture,
        fullName: `${data.firstName}${data.lastName}`,
        googleId: data.id,
      };
      const createdUser = new this.usersModel(createData);
      const newUser = await createdUser.save();
      await this.userAccessesService.createOne({
        data: {
          userId: newUser._id,
          roleName: roleName.BASIC,
          status: UserStatus.ACTIVE,
        },
        credentials: {
          isAdmin: true,
        },
      });

      token = this.authService.createUserToken({ userId: newUser._id });

      return token;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async githubLogin(data: any): Promise<{ token: string }> {
    try {
      const user = await this.usersModel
        .findOne({
          githubId: data.id,
        })
        .exec();

      let token;
      if (user?.status === UserStatus.INACTIVE) {
        return Promise.reject({
          name: 'InactiveUser',
          code: 403,
        });
      }

      if (user?.status === UserStatus.ACTIVE) {
        token = this.authService.createUserToken({ userId: user._id });

        return token;
      }

      const createData = {
        status: UserStatus.ACTIVE,
        githubId: data.id,
        profilePhoto: data.profileUrl,
        fullName: data.displayName,
      };
      const createdUser = new this.usersModel(createData);
      const newUser = await createdUser.save();
      await this.userAccessesService.createOne({
        data: {
          userId: newUser._id,
          roleName: roleName.BASIC,
          status: UserStatus.ACTIVE,
        },
        credentials: {
          isAdmin: true,
        },
      });

      token = this.authService.createUserToken({ userId: newUser._id });

      return token;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getMe({ query, credentials }: GetMeService): Promise<User | undefined> {
    const user = await this.userAccessesService.findOne({
      query: {
        userId: query.userId,
      },
      credentials,
    });

    return user;
  }

  async changePassWord({
    data,
    credentials,
  }: ChangePassWordService): Promise<boolean> {
    try {
      const user = await this.usersModel
        .findOne({
          _id: credentials.userId,
        })
        .select(['username', 'password'])
        .exec();

      if (!user) {
        return Promise.reject({
          name: 'UserNotFound',
          code: 404,
        });
      }

      validateAccessToSingle({
        data: user,
        credentials,
      });

      const comparePassword = bcrypt.compareSync(
        data.oldPassword,
        user.password,
      );

      if (!comparePassword) {
        return Promise.reject({
          name: 'PasswordIsIncorrect',
          code: 400,
        });
      }
      const hashPass = await bcrypt.hash(
        data.newPassword,
        this.configService.bcryptSalt,
      );

      Object.assign(user, {
        password: hashPass,
      });

      await user.save();

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async resetPassWord({ data }: ResetPassWordService): Promise<boolean> {
    try {
      const user = await this.usersModel
        .findOne({
          email: data.email,
        })
        .select(['username', 'password', 'email'])
        .exec();

      if (!user) {
        return Promise.reject({
          name: 'UserNotFound',
          code: 404,
        });
      }

      if (!user?.username || !user?.password) {
        return Promise.reject({
          name: 'User Not login with userName',
          code: 400,
        });
      }

      const getOtp = await this.otpsService.findOne({
        query: { userId: user._id },
        credentials: {
          isAdmin: true,
        },
      });

      if (getOtp.otp !== data.otp) {
        return Promise.reject({
          name: 'OtpIsIncorrect',
          code: 400,
        });
      }

      const hashPass = await bcrypt.hash(
        data.newPassword,
        this.configService.bcryptSalt,
      );

      Object.assign(user, {
        password: hashPass,
      });

      await user.save();

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findMany({
    query,
    credentials,
  }: FindManyUsersService): Promise<FindManyUsersResult> {
    try {
      const {
        limit,
        offset = 0,
        sortBy = '_id',
        ...queryWithoutSortByAndLimit
      } = query;

      const promises = [];

      const { sortingCondition, findingQuery, findAllQuery, hasPage } =
        buildFindingQuery({
          query: {
            ...queryWithoutSortByAndLimit,
            sortBy,
            limit,
          },
        });

      if (hasPage) {
        promises.push(
          this.usersModel
            .find(findingQuery)
            .sort(sortingCondition)
            .skip(offset)
            .limit(limit)
            .exec(),
          this.usersModel.countDocuments(findAllQuery).exec(),
        );
      }

      if (!hasPage) {
        promises.push(
          this.usersModel.find(findingQuery).sort(sortingCondition).exec(),
          this.usersModel.countDocuments(findAllQuery).exec(),
        );
      }

      const [users, totalCount] = await Promise.all(promises);

      if (!users || !users.length) {
        return {
          totalCount,
          list: [],
        };
      }

      const { validData } = validateAccessToList({
        data: users,
        credentials,
      });

      return {
        totalCount,
        list: validData,
      };
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async updateOne({
    data,
    query,
    credentials,
  }: UpdateOneUsersService): Promise<User> {
    try {
      const user = await this.usersModel.findOne(query).exec();

      if (!user) {
        return Promise.reject({
          name: 'UserNotFound',
          code: 404,
        });
      }

      validateAccessToSingle({
        data: user,
        credentials,
      });

      Object.assign(user, data);

      await user.save();

      return user;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async deleteOne({
    query,
    credentials,
  }: DeleteOneUsersService): Promise<boolean> {
    try {
      const user = await this.usersModel.findOne(query).exec();

      if (!user) {
        return Promise.reject({
          name: 'UserNotFound',
          code: 404,
        });
      }

      validateAccessToSingle({
        data: user,
        credentials,
      });

      await Promise.all([
        this.usersModel.deleteOne(query).exec(),
        this.userAccessesService.deleteMany({
          query: {
            userId: user._id,
          },
          credentials,
        }),
        this.otpsService.deleteMany({
          query: {
            userId: user._id,
          },
          credentials,
        }),
      ]);

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
