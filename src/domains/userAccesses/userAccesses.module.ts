import { Module } from '@nestjs/common';
import { UserAccessesController } from './userAccesses.controller';
import { UserAccessesService } from './userAccesses.service';
import { ConfigModule } from '../../configs/configs.module';
import { UserAccessesSchema } from './models/userAccesses.schema';
import { DatabaseModule } from '../../database/database.module';
import { buildSchemaProvider } from '../../helpers/database';

const UserAccessesProvider = buildSchemaProvider({
  name: 'UserAccesses',
  schema: UserAccessesSchema,
});

@Module({
  imports: [ConfigModule, DatabaseModule],
  controllers: [UserAccessesController],
  providers: [UserAccessesService, UserAccessesProvider],
  exports: [UserAccessesService],
})
export class UserAccessModule {}
