import * as mongoose from 'mongoose';
import { roleName, UserAccessStatus } from '../../../common/enum';
import { Types } from 'mongoose';

export const UserAccessesSchema = new mongoose.Schema(
  {
    userId: { type: Types.ObjectId, ref: 'Users' },
    roleName: {
      type: String,
      enum: Object.values(roleName),
      require: true,
      default: roleName.BASIC,
    },
    status: {
      type: String,
      enum: Object.values(UserAccessStatus),
      default: UserAccessStatus.PENDING,
    },
  },
  {
    versionKey: false,
    timestamps: true,
  },
);
