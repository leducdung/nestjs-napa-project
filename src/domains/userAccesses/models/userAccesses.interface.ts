import { Document, Types } from 'mongoose';
import { ICredential } from '../../../shared/validateAccess';
import { roleName } from '../../../common/enum';

export interface UserAccess extends Document {
  userId: Types.ObjectId;
  roleName: roleName;
  status: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface CreateUserAccessService {
  data: CreateUserAccessData;
  credentials: ICredential;
}

interface CreateUserAccessData {
  userId: Types.ObjectId;
  roleName: roleName;
  status?: string;
}

export enum SortBy {
  id = '_id',
  createdAt = 'createdAt',
  updatedAt = 'updatedAt',
}

export interface FindManyQuery {
  userId?: Types.ObjectId;
  roleName?: roleName;
  status?: string;
  createdAt?: Date;
  updatedAt?: Date;
  sortBy?: string;
  offset?: number;
  cursor?: string;
  limit?: number;
  sortDirection?: string;
}

export interface FindManyUserAccessesResult {
  totalCount: number;
  list: UserAccess[];
}

export interface FindManyUserAccessesService {
  query: FindManyQuery;
  credentials: ICredential;
}

export interface FindOneQuery {
  _id?: Types.ObjectId;
  userId?: Types.ObjectId | string;
}

export interface FindOneUserAccessService {
  query: FindOneQuery;
  credentials: ICredential;
}

export interface UpdateOneUserAccessData {
  roleName?: roleName;
  status?: string;
}

export interface UpdateOneUserAccessService {
  data: UpdateOneUserAccessData;
  credentials: ICredential;
  query: FindOneQuery;
}

export interface DeleteOneUserAccessesService {
  query: FindOneQuery;
  credentials: ICredential;
}
