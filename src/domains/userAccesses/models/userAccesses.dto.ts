import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { Types } from 'mongoose';
import {
  IsOptional,
  IsEnum,
  IsString,
  IsNumber,
  IsNotEmpty,
  IsPositive,
  Min,
  IsMongoId,
} from 'class-validator';
import { SortBy } from './userAccesses.interface';
import { roleName, SortDirection } from '../../../common/enum';

export class CreateOneUserAccessDto {
  @ApiProperty({ type: 'Types.ObjectId' })
  @IsNotEmpty()
  @IsMongoId()
  readonly userId: Types.ObjectId;

  @ApiProperty({ enum: roleName })
  @IsString()
  @IsEnum(roleName)
  @IsNotEmpty()
  readonly roleName: roleName;
}

export class UpdateUserAccessDto {
  @ApiPropertyOptional({ type: 'Types.ObjectId' })
  @IsNotEmpty()
  @IsMongoId()
  readonly userId?: Types.ObjectId;

  @ApiPropertyOptional({ enum: roleName })
  @IsString()
  @IsEnum(roleName)
  @IsNotEmpty()
  readonly roleName?: roleName;
}

export class ParamFindUserAccess {
  @IsMongoId()
  @IsNotEmpty()
  readonly userAccessId: Types.ObjectId;
}

const sortBy = [SortBy.id, SortBy.createdAt, SortBy.updatedAt];

export class FindManyUserAccessDto {
  @ApiPropertyOptional({ enum: sortBy })
  @IsEnum(sortBy)
  @IsOptional()
  readonly sortBy?: SortBy = SortBy.id;

  @ApiPropertyOptional({ enum: SortDirection })
  @IsEnum(SortDirection)
  @IsOptional()
  readonly sortDirection?: SortDirection = SortDirection.asc;

  @ApiPropertyOptional()
  @IsNumber()
  @IsOptional()
  @IsPositive()
  @Type(() => Number)
  readonly limit?: number;

  @ApiPropertyOptional()
  @IsNumber()
  @IsOptional()
  @Min(0)
  @Type(() => Number)
  readonly offset?: number;

  @ApiPropertyOptional({ type: 'Types.ObjectId' })
  @IsOptional()
  @IsMongoId()
  readonly userId?: Types.ObjectId;

  @ApiPropertyOptional({ enum: roleName })
  @IsString()
  @IsEnum(roleName)
  @IsOptional()
  readonly roleName?: roleName;
}
