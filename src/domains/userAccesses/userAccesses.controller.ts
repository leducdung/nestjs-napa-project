import {
  Controller,
  Post,
  Body,
  Get,
  Query,
  Param,
  Put,
  Delete,
  Request,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { checkControllerErrors } from '../../shared/checkErrors';
import {
  FindManyUserAccessDto,
  CreateOneUserAccessDto,
  ParamFindUserAccess,
  UpdateUserAccessDto,
} from './models/userAccesses.dto';
import {
  FindManyUserAccessesResult,
  UserAccess,
} from './models/userAccesses.interface';
import { UserAccessesService } from './userAccesses.service';
import { JwtAuthGuard } from '../../middlewares/auth/jwt-auth.guard';
import { Scopes } from '../../middlewares/authen/authen';
import { roleName } from '../../common/enum';

@ApiTags('UserAccesses')
@Controller('userAccesses')
export class UserAccessesController {
  constructor(private readonly userAccessesService: UserAccessesService) {}

  @Post('')
  @UseGuards(JwtAuthGuard, new Scopes([roleName.ADMIN]))
  @ApiOperation({ description: 'To create user access' })
  async createOne(
    @Body() createOneUserAccessDto: CreateOneUserAccessDto,
    @Request() { user },
  ) {
    try {
      const newPhotos = await this.userAccessesService.createOne({
        data: createOneUserAccessDto,
        credentials: user,
      });

      return newPhotos;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Get('/')
  @UseGuards(JwtAuthGuard, new Scopes([roleName.ADMIN]))
  @ApiOperation({ description: 'To Get many user access' })
  async findMany(
    @Query() query: FindManyUserAccessDto,
    @Request() { user },
  ): Promise<FindManyUserAccessesResult> {
    try {
      const userAccesses = await this.userAccessesService.findMany({
        query,
        credentials: user,
      });

      return userAccesses;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Get('/:userAccessesId')
  @UseGuards(JwtAuthGuard, new Scopes([roleName.ADMIN]))
  @ApiOperation({ description: 'To Get one user access' })
  async findOne(
    @Param() { userAccessId }: ParamFindUserAccess,
    @Request() { user },
  ): Promise<UserAccess> {
    try {
      const userAccess = await this.userAccessesService.findOne({
        query: {
          _id: userAccessId,
        },
        credentials: user,
      });

      return userAccess;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Put('/:userAccessesId')
  @UseGuards(JwtAuthGuard, new Scopes([roleName.ADMIN]))
  @ApiOperation({ description: 'To update user access' })
  async updateOne(
    @Param() { userAccessId }: ParamFindUserAccess,
    @Body() updateUserAccessDto: UpdateUserAccessDto,
    @Request() { user },
  ): Promise<UserAccess> {
    try {
      const userAccess = await this.userAccessesService.updateOne({
        data: updateUserAccessDto,
        query: { _id: userAccessId },
        credentials: user,
      });

      return userAccess;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Delete('/:userAccessesId')
  @UseGuards(JwtAuthGuard, new Scopes([roleName.ADMIN]))
  @ApiOperation({ description: 'To delete user access' })
  async deleteOne(
    @Param() { userAccessId }: ParamFindUserAccess,
    @Request() { user },
  ): Promise<boolean> {
    try {
      const userAccess = await this.userAccessesService.deleteOne({
        query: { _id: userAccessId },
        credentials: user,
      });

      return userAccess;
    } catch (error) {
      checkControllerErrors(error);
    }
  }
}
