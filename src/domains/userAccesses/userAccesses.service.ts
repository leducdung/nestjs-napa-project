import {
  CreateUserAccessService,
  DeleteOneUserAccessesService,
  FindManyUserAccessesResult,
  FindManyUserAccessesService,
  FindOneUserAccessService,
  UpdateOneUserAccessService,
  UserAccess,
} from './models/userAccesses.interface';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { UserAccessStatus } from '../../common/enum';
import { buildFindingQuery } from '../../helpers/build';
import {
  validateAccessToList,
  validateAccessToSingle,
} from '../../shared/validateAccess';

@Injectable()
export class UserAccessesService {
  constructor(
    @Inject('UserAccesses')
    private readonly userAccessModel: Model<UserAccess>,
  ) {}

  async createOne({
    data,
    credentials,
  }: CreateUserAccessService): Promise<UserAccess> {
    try {
      const userAccess = new this.userAccessModel({
        ...data,
        status: UserAccessStatus.ACCEPTED,
      });

      validateAccessToSingle({
        data: userAccess,
        credentials,
      });

      await userAccess.save();

      return userAccess;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findMany({
    query,
    credentials,
  }: FindManyUserAccessesService): Promise<FindManyUserAccessesResult> {
    try {
      const {
        limit,
        offset = 0,
        sortBy = '_id',
        ...queryWithoutSortByAndLimit
      } = query;

      const promises = [];

      const { sortingCondition, findingQuery, findAllQuery, hasPage } =
        buildFindingQuery({
          query: {
            ...queryWithoutSortByAndLimit,
            sortBy,
            limit,
          },
        });

      if (hasPage) {
        promises.push(
          this.userAccessModel
            .find(findingQuery)
            .populate('userId')
            .sort(sortingCondition)
            .skip(offset)
            .limit(limit)
            .exec(),
          this.userAccessModel.countDocuments(findAllQuery).exec(),
        );
      }

      if (!hasPage) {
        promises.push(
          this.userAccessModel
            .find(findingQuery)
            .populate('userId')
            .sort(sortingCondition)
            .exec(),
          this.userAccessModel.countDocuments(findAllQuery).exec(),
        );
      }

      const [userAccesses, totalCount] = await Promise.all(promises);

      if (!userAccesses || !userAccesses.length) {
        return {
          totalCount,
          list: [],
        };
      }

      const { validData } = validateAccessToList({
        data: userAccesses,
        credentials,
      });

      return {
        totalCount,
        list: validData,
      };
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findOne({
    query,
    credentials,
  }: FindOneUserAccessService): Promise<UserAccess> {
    try {
      const userAccess = await this.userAccessModel
        .findOne(query)
        .populate('userId')
        .exec();

      if (!userAccess) {
        return Promise.reject({
          name: 'UserAccessNotFound',
          code: 404,
        });
      }

      validateAccessToSingle({
        data: userAccess,
        credentials,
      });

      return userAccess;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async updateOne({
    data,
    query,
    credentials,
  }: UpdateOneUserAccessService): Promise<UserAccess> {
    try {
      const userAccess = await this.userAccessModel.findOne(query).exec();

      if (!userAccess) {
        return Promise.reject({
          name: 'UserAccessNotFound',
          code: 404,
        });
      }

      validateAccessToSingle({
        data: userAccess,
        credentials,
      });

      Object.assign(userAccess, data);

      await userAccess.save();

      return userAccess;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async deleteOne({
    query,
    credentials,
  }: DeleteOneUserAccessesService): Promise<boolean> {
    try {
      const userAccess = await this.userAccessModel.findOne(query).exec();

      if (!userAccess) {
        return Promise.reject({
          name: 'UserAccessNotFound',
          code: 404,
        });
      }

      validateAccessToSingle({
        data: userAccess,
        credentials,
      });

      await this.userAccessModel.deleteOne(query).exec();

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async deleteMany({ query, credentials }) {
    try {
      const userAccesses = await this.userAccessModel.find(query).exec();

      if (!userAccesses?.length) {
        return true;
      }

      const { validDataLength } = validateAccessToList({
        data: userAccesses,
        credentials,
      });

      if (validDataLength !== userAccesses.length) {
        return Promise.reject({
          name: 'validate err',
          code: 403,
        });
      }

      await this.userAccessModel.deleteMany(query).exec();

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
