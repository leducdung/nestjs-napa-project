import { Module } from '@nestjs/common';
import { SampleController } from './samples.controller';
import { SampleService } from './samples.service';

@Module({
  imports: [],
  controllers: [SampleController],
  providers: [SampleService],
})
export class SampleModule {}
