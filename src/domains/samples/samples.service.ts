import { Injectable } from '@nestjs/common';
import { ConfigService } from '../../configs/configs.service';
const configService = new ConfigService();

@Injectable()
export class SampleService {
  getHello(): string {
    console.log(`Hello World! ${configService.internalPort}`);

    return `Hello World! ${configService.env}`;
  }
}
