import { forwardRef, Module } from '@nestjs/common';
import { OtpsController } from './otps.controller';
import { OtpsService } from './otps.service';
import { ConfigModule } from '../../configs/configs.module';
import { OtpsSchema } from './models/otps.schema';
import { DatabaseModule } from '../../database/database.module';
import { buildSchemaProvider } from '../../helpers/database';
import { SendGridService } from '../mail/sendgrid.service';
import { UserModule } from '../users/users.module';
import { OtpsCombinedService } from './otps.combind.service';

const OtpsProvider = buildSchemaProvider({
  name: 'Otps',
  schema: OtpsSchema,
});

@Module({
  imports: [ConfigModule, forwardRef(() => UserModule), DatabaseModule],
  controllers: [OtpsController],
  providers: [OtpsService, SendGridService, OtpsCombinedService, OtpsProvider],
  exports: [OtpsService, OtpsCombinedService],
})
export class OtpModule {}
