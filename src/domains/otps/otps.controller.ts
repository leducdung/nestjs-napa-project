import { Controller, Body, Put } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { checkControllerErrors } from '../../shared/checkErrors';
import { GetOtpResetPassword } from './models/otps.dto';
import { OtpsCombinedService } from './otps.combind.service';

@ApiTags('Otps')
@Controller('otps')
export class OtpsController {
  constructor(private readonly otpsCombinedService: OtpsCombinedService) {}

  @Put('reset/password')
  @ApiOperation({ description: 'To create otp reset password' })
  async createOne(@Body() createOneOtpDto: GetOtpResetPassword) {
    try {
      const newPhotos =
        await this.otpsCombinedService.createOtpAndSendMailResetPassword({
          data: createOneOtpDto,
        });

      return newPhotos;
    } catch (error) {
      checkControllerErrors(error);
    }
  }
}
