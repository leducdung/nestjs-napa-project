import { Injectable } from '@nestjs/common';
import { buildOTPEmailData } from '../../helpers/build';
import { SendGridService } from '../mail/sendgrid.service';
import { UsersService } from '../users/users.service';
import { OtpsService } from './otps.service';

@Injectable()
export class OtpsCombinedService {
  constructor(
    private readonly otpsService: OtpsService,
    private readonly usersService: UsersService,
    private readonly sendGridService: SendGridService,
  ) {}

  async createOtpAndSendMailResetPassword({ data }): Promise<boolean> {
    try {
      const user = await this.usersService.findOne({
        query: {
          email: data.email,
        },
        needToCheckExist: false,
        needToValidateAccess: false,
        credentials: {
          isAdmin: true,
        },
      });

      if (!user) {
        return Promise.reject({
          name: 'UserNotFound',
          code: 404,
        });
      }

      const userOpt = await this.otpsService.findOne({
        query: { userId: user._id },
        credentials: {
          isAdmin: true,
        },
        needToCheckExist: false,
      });

      const randomOtp = Math.random().toString(36).substring(7);
      const expirationTime = new Date().getTime() + 1000 * 60 * 5;
      const createData = {
        otp: randomOtp,
        userId: user._id,
        expirationTime: new Date(expirationTime).toISOString(),
      };

      const mail = buildOTPEmailData({
        to: data.email,
        subject: 'reset passwpord',
        text: 'reset passwpord',
        typeEmail: 'reset passwpord',
        otp: randomOtp,
      });

      // await this.sendGridService.send(mail);

      if (userOpt) {
        Object.assign(userOpt, createData);
        await userOpt.save();

        return true;
      }

      await this.otpsService.createOne({
        data: createData,
      });

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
