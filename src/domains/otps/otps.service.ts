import {
  CreateOtpService,
  DeleteOneOtpsService,
  FindManyOtpsResult,
  FindManyOtpsService,
  FindOneOtpService,
  UpdateOneOtpService,
  Otp,
} from './models/otps.interface';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { buildFindingQuery } from '../../helpers/build';
import {
  validateAccessToList,
  validateAccessToSingle,
} from '../../shared/validateAccess';

@Injectable()
export class OtpsService {
  constructor(
    @Inject('Otps')
    private readonly otpModel: Model<Otp>,
  ) {}

  async createOne({ data }: CreateOtpService): Promise<Otp> {
    try {
      const userOpt = await this.otpModel
        .findOne({
          userId: data.userId,
        })
        .exec();

      if (userOpt) {
        Object.assign(userOpt, data);
        userOpt.save();

        return userOpt;
      }

      const created = await new this.otpModel(data).save();
      return created;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findMany({
    query,
    credentials,
  }: FindManyOtpsService): Promise<FindManyOtpsResult> {
    try {
      const {
        limit,
        offset = 0,
        sortBy = '_id',
        ...queryWithoutSortByAndLimit
      } = query;

      const promises = [];

      const { sortingCondition, findingQuery, findAllQuery, hasPage } =
        buildFindingQuery({
          query: {
            ...queryWithoutSortByAndLimit,
            sortBy,
            limit,
          },
        });

      if (hasPage) {
        promises.push(
          this.otpModel
            .find(findingQuery)
            .populate('companyID')
            .populate('departmentID')
            .populate('projectID')
            .populate('createdBy')
            .sort(sortingCondition)
            .skip(offset)
            .limit(limit)
            .exec(),
          this.otpModel.countDocuments(findAllQuery).exec(),
        );
      }

      if (!hasPage) {
        promises.push(
          this.otpModel
            .find(findingQuery)
            .populate('userId')
            .sort(sortingCondition)
            .exec(),
          this.otpModel.countDocuments(findAllQuery).exec(),
        );
      }

      const [otps, totalCount] = await Promise.all(promises);

      if (!otps || !otps.length) {
        return {
          totalCount,
          list: [],
        };
      }

      const { validData } = validateAccessToList({
        data: otps,
        credentials,
      });

      return {
        totalCount,
        list: validData,
      };
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findOne({
    query,
    credentials,
    needToCheckExist = true,
  }: FindOneOtpService): Promise<Otp> {
    try {
      const otp = await this.otpModel.findOne(query).exec();

      if (!otp && needToCheckExist) {
        return Promise.reject({
          name: 'OtpNotFound',
          code: 404,
        });
      }

      validateAccessToSingle({
        data: otp,
        credentials,
      });

      return otp;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async updateOne({
    data,
    query,
    credentials,
  }: UpdateOneOtpService): Promise<Otp> {
    try {
      const otp = await this.otpModel.findOne(query).exec();

      if (!otp) {
        return Promise.reject({
          name: 'OtpNotFound',
          code: 404,
        });
      }

      validateAccessToSingle({
        data: otp,
        credentials,
      });

      Object.assign(otp, data);

      await otp.save();

      return otp;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async deleteOne({
    query,
    credentials,
  }: DeleteOneOtpsService): Promise<boolean> {
    try {
      const otp = await this.otpModel.findOne(query).exec();

      if (!otp) {
        return Promise.reject({
          name: 'OtpNotFound',
          code: 404,
        });
      }

      validateAccessToSingle({
        data: otp,
        credentials,
      });

      await this.otpModel.deleteOne(query).exec();

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async deleteMany({ query, credentials }) {
    try {
      const otps = await this.otpModel.find(query).exec();

      if (!otps?.length) {
        return true;
      }

      const { validDataLength } = validateAccessToList({
        data: otps,
        credentials,
      });

      if (validDataLength !== otps.length) {
        return Promise.reject({
          name: 'validate err',
          code: 403,
        });
      }

      await this.otpModel.deleteMany(query).exec();

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
