import { Document, Types } from 'mongoose';
import { ICredential } from '../../../shared/validateAccess';
import { roleName } from '../../../common/enum';

export interface Otp extends Document {
  userId: Types.ObjectId;
  otp: string;
  expirationTime: Date;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface CreateOtpService {
  data: CreateOtpData;
}

interface CreateOtpData {
  userId?: Types.ObjectId;
  otp?: string;
  expirationTime?: Date | string;
  email?: string;
}

export enum SortBy {
  id = '_id',
  createdAt = 'createdAt',
  updatedAt = 'updatedAt',
}

export interface FindManyQuery {
  userId?: Types.ObjectId;
  otp?: string;
  expirationTime?: Date;
  createdAt?: Date;
  updatedAt?: Date;
  sortBy?: string;
  offset?: number;
  cursor?: string;
  limit?: number;
  sortDirection?: string;
}

export interface FindManyOtpsResult {
  totalCount: number;
  list: Otp[];
}

export interface FindManyOtpsService {
  query: FindManyQuery;
  credentials: ICredential;
}

export interface FindOneQuery {
  _id?: Types.ObjectId;
  userId?: Types.ObjectId | string;
}

export interface FindOneOtpService {
  query: FindOneQuery;
  credentials: ICredential;
  needToCheckExist?: boolean;
}

export interface UpdateOneOtpData {
  otp?: string;
  expirationTime?: Date;
}

export interface UpdateOneOtpService {
  data: UpdateOneOtpData;
  credentials: ICredential;
  query: FindOneQuery;
}

export interface DeleteOneOtpsService {
  query: FindOneQuery;
  credentials: ICredential;
}
