import * as mongoose from 'mongoose';
import { Types } from 'mongoose';

export const OtpsSchema = new mongoose.Schema(
  {
    userId: { type: Types.ObjectId, ref: 'Users' },
    otp: { type: String, default: null },
    expirationTime: { type: Date },
  },
  {
    versionKey: false,
    timestamps: true,
  },
);
