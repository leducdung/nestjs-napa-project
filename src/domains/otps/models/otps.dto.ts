import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, Length } from 'class-validator';

export class GetOtpResetPassword {
  @ApiProperty({
    description: 'Unique email address',
    required: true,
    example: 'sample@sample.com',
  })
  @IsNotEmpty()
  @Length(1, 320)
  @IsEmail()
  email: string;
}
