import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../../domains/users/users.service';
import { JwtPayload } from './models/auth.interface';

@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService) {}

  async login(user: any) {
    const payload = { username: user.username, sub: user.userId };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async createUserToken(payload: JwtPayload): Promise<{ token: string }> {
    try {
      const token = await this.jwtService.signAsync(payload, {
        expiresIn: '2d',
      });

      return { token };
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
