import {
  BadRequestException,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { UserStatus } from '../../common/enum';
import { IS_PUBLIC_KEY } from '../../shared/customDecorator';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  private isPublic: boolean;

  constructor(private reflector: Reflector) {
    super();

    this.isPublic = false;
  }

  canActivate(context: ExecutionContext) {
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);

    this.isPublic = isPublic;

    return super.canActivate(context);
  }

  /**
   * Case 1: Endpoint is private and token not valid or not specified token
   * Case 2: Endpoint is Public. (not required token)
   * Case 3: Endpoint is private and token is valid
   */
  handleRequest(err: any, user: any): any {
    if (err || (!user && !this.isPublic) || !user.roleNames?.length) {
      throw new UnauthorizedException();
    }
    if (user.status === UserStatus.INACTIVE) {
      throw new BadRequestException('INACTIVE ACCOUNT');
    }

    return user
      ? { ...user, isPublic: this.isPublic }
      : { isPublic: this.isPublic };
  }
}
