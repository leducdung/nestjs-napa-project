import { forwardRef, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '../../configs/configs.module';
import { ConfigService } from '../../configs/configs.service';
import { AuthService } from './auth.service';
import { UserModule } from '../../domains/users/users.module';
import { UserAccessModule } from '../../domains/userAccesses/userAccesses.module';
import { UsersService } from '../../domains/users/users.service';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './local.strategy';
import { jwtConstants } from './constants';
import { JwtStrategy } from './jwt.strategy';

@Module({
  imports: [
    forwardRef(() => UserModule),
    forwardRef(() => UserAccessModule),
    forwardRef(() =>
      JwtModule.register({
        secret: jwtConstants.secret,
        signOptions: { expiresIn: '7d' },
      }),
    ),
    forwardRef(() => ConfigModule),
    forwardRef(() => PassportModule),
  ],
  controllers: [],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}
