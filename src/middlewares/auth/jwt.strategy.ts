import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { jwtConstants } from './constants';
import { UsersService } from '../../domains/users/users.service';
import { UserAccessesService } from '../../domains/userAccesses/userAccesses.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly usersService: UsersService,
    private readonly userAccessesService: UserAccessesService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }

  async validate(payload: any) {
    const [accesses, userInfo] = await Promise.all([
      this.userAccessesService.findMany({
        query: {
          userId: payload.userId,
        },
        credentials: {
          isAdmin: true,
        },
      }),
      this.usersService.findOne({
        query: {
          _id: payload.userId,
        },
        credentials: {
          isAdmin: true,
        },
        needToCheckExist: false,
        needToValidateAccess: false,
      }),
    ]);

    const roleNames = accesses.list.map((access) => access.roleName);

    return {
      roleNames,
      userId: userInfo._id.toString(),
      status: userInfo.status,
      email: userInfo.email,
      firstName: userInfo.firstName,
      lastName: userInfo.lastName,
      fullName: userInfo.fullName,
    };
  }
}
