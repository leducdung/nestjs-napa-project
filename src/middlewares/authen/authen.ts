import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { roleName } from '../../common/enum';

@Injectable()
export class Scopes implements CanActivate {
  constructor(private readonly requiredScopes: string[]) {}

  canActivate(context: ExecutionContext): boolean {
    const requiredScopes = this.requiredScopes;
    const req = context.switchToHttp().getRequest();
    const user = req.user;
    let isAdmin: boolean;

    if (!requiredScopes?.length) {
      // checking user is logged in with admin account in public url/route
      if (user?.permissions?.length) {
        const adminInfo = req.user.roleNames.find(
          (each) => each === roleName.ADMIN,
        );
        if (adminInfo) {
          isAdmin = true;
        }
      }
      req.user = user
        ? {
            userId: user.userId,
            email: user.email,
            isPublic: true,
            accountType: user.accountType,
            isAdmin,
          }
        : { isPublic: true };

      return true;
    }

    if (!user?.roleNames?.length) {
      return false;
    }

    const { roleNames, ...userReq } = req.user;

    const adminRole = roleNames.find((each) => each === roleName.ADMIN);

    if (adminRole) {
      req.user = {
        isAdmin: true,
        ...userReq,
        roleNames,
      };

      return true;
    }

    // check roleName
    const hasRoles = roleNames.some((permission) =>
      requiredScopes.includes(permission),
    );

    if (!hasRoles) {
      return false;
    }

    return hasRoles;
  }
}
