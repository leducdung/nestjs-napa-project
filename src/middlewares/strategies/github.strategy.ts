import { ConfigService } from './../../configs/configs.service';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Profile, Strategy } from 'passport-github';

@Injectable()
export class GithubOauthStrategy extends PassportStrategy(Strategy, 'github') {
  constructor(private configService: ConfigService) {
    super({
      clientID: configService.gitHubClientId,
      clientSecret: configService.gitHubClientSecret,
      callbackURL: '/auth/github/callback',
      scope: ['public_profile'],
    });
  }

  async validate(accessToken: string, _refreshToken: string, profile: Profile) {
    // const { id } = profile;
    // const user = await this.usersService.findOrCreate(id, 'github');
    // if (!user) {
    //   throw new UnauthorizedException();
    // }
    return profile;
  }
}
