import { roleName } from '../common/enum';

export interface ICredential {
  userId?: string;
  email?: string;
  isAdmin?: boolean;
  isPublic?: boolean;
  roleNames?: roleName;
}

export interface IValidateAccess {
  data;
  credentials: ICredential;
}

export const validateAccessToSingle = ({
  data,
  credentials,
}: IValidateAccess) => {
  if (!credentials || Array.isArray(data)) {
    throw {
      code: 403,
      name: 'ValidationError',
    };
  }

  const { userId, isAdmin, isPublic, roleNames }: ICredential = credentials;

  if (!data || isAdmin || isPublic || roleNames.includes(roleName.ADMIN)) {
    return data;
  }

  const ownerIds = [
    data._id,
    data._id?.userId,
    data?.createdBy,
    data.createdBy?._id,
    data?.userId?._id,
    data.userId,
  ].map((each) => {
    if (!each) {
      return;
    }

    return each.toString();
  });

  if (ownerIds.includes(userId)) {
    return data;
  }

  throw {
    code: 403,
    name: 'ValidationError',
  };
};

export const validateAccessToList = ({
  data,
  credentials,
}: IValidateAccess) => {
  if (!credentials) {
    throw {
      code: 403,
      name: 'ValidationError',
    };
  }

  const { userId, isAdmin, isPublic, roleNames }: ICredential = credentials;

  if (
    !data?.length ||
    isAdmin ||
    isPublic ||
    roleNames.includes(roleName.ADMIN)
  ) {
    return {
      validData: data,
      validDataLength: data?.length || 0,
    };
  }

  let validDataLength = 0;

  const validData = data.map((each) => {
    const ownerIds = [
      each._id,
      each._id?.userId,
      each?.createdBy,
      each.createdBy?._id,
      each?.userId?._id,
      each.userId,
    ].map((owner) => {
      if (!owner) {
        return;
      }

      return owner.toString();
    });

    if (ownerIds.includes(userId)) {
      validDataLength++;

      return each;
    }

    return null;
  });

  return {
    validData,
    validDataLength,
  };
};
