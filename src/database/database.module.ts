import { Module } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { ConfigService } from '../configs/configs.service';
import { DatabaseProviderName } from '../common/constants';

const configService = new ConfigService();

const DatabaseProviders = [
  {
    provide: DatabaseProviderName,
    useFactory: () => {
      if (global.databaseConnection) {
        return new Promise((resolve) => resolve(global.databaseConnection));
      }
      console.log(configService.dbURI);
      const databaseConnection = mongoose.connect(configService.dbURI);

      console.log(configService.dbURI);

      global.databaseConnection = databaseConnection;

      return databaseConnection;
    },
  },
];

@Module({
  providers: DatabaseProviders,
  exports: DatabaseProviders,
})
export class DatabaseModule {}
