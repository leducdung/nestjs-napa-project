export enum UserGender {
  MALE = 'MALE',
  FEMALE = 'FEMALE',
  OTHERS = 'OTHERS',
}

export enum UserStatus {
  INACTIVE = 'INACTIVE',
  ACTIVE = 'ACTIVE',
}

export enum UserAccessStatus {
  ACCEPTED = 'ACCEPTED',
  REJECTED = 'REJECTED',
  PENDING = 'PENDING',
}

export enum roleName {
  ADMIN = 'ADMIN',
  BASIC = 'BASIC',
}

export enum SortDirection {
  asc = 'ASC',
  desc = 'DESC',
}
