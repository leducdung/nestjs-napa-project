export const notFoundErrors = {
  UserNotFound: 'User was not found',
  UserNameNotFound: 'UserName Not Found!',
  OtpNotFound: 'Otp Not Found',
};

export const badRequestErrors = {
  EmailExists: 'Email exists!',
  EmailOrUserNameExists: 'Email Or User Name Exists!',
  PasswordIsIncorrect: 'Password Is Incorrect!',
  OtpIsIncorrect: 'Otp Is Incorrect',
};

export const forbiddenErrors = {
  ValidationError: 'No permissions',
  InactiveUser: 'Inactive User',
};
