import * as dotenv from 'dotenv';
import * as Joi from 'joi';
import * as fs from 'fs';

import { DEVELOPMENT, TEST, PRODUCTION, LOCAL } from '../common/constants';

export interface EnvConfig {
  [key: string]: string;
}

// Note: default env is development.
export class ConfigService {
  private readonly envConfig: EnvConfig;

  constructor() {
    const nodeEnv = process?.env?.NODE_ENV || LOCAL;
    console.log(process?.env?.INTERNAL_HOST);

    const configFilePath = `environments/.${nodeEnv.toLocaleLowerCase()}.env`;

    const hasEnvFile = fs.existsSync(configFilePath);

    if (!hasEnvFile) {
      throw Error(`environment <${nodeEnv}> file not exists!`);
    }

    const config = dotenv.parse(fs.readFileSync(configFilePath));

    this.envConfig = this.validateInput({
      ...config,
      NODE_ENV: nodeEnv,
    });
  }

  /**
   * Ensures all needed variables are set, and returns the validated JavaScript object
   * including the applied default values.
   */
  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      // node env
      NODE_ENV: Joi.string()
        .valid(DEVELOPMENT, PRODUCTION, TEST, LOCAL)
        .default(LOCAL),
      ENV: Joi.string().required(),

      // host && port
      INTERNAL_HOST: Joi.string().required(),
      INTERNAL_PORT: Joi.number().required(),

      // mongose
      DATABASE_USER: Joi.string().required(),
      DATABASE_PASSWORD: Joi.string().required(),
      DATABASE_HOST: Joi.string().required(),
      DATABASE_PORT: Joi.number().required(),
      DATABASE_NAME: Joi.string().required(),
      // jwt
      JWT_COMFIRM_SECRET: Joi.string().required(),
      JWT_SECRET: Joi.string().required(),

      // bcrypt
      BCRYPT_SALT: Joi.number().required(),

      //host
      FE_HOST: Joi.string().required(),
      BE_HOST: Joi.string().required(),

      //Google
      GOOGLE_CLIENT_ID: Joi.string().required(),
      GOOGLE_CLIENT_SECRET: Joi.string().required(),
      GOOGLE_CALLBACK: Joi.string().required(),

      //github
      GITHUB_CLIENT_ID: Joi.string().required(),
      GITHUB_CLIENT_SECRET: Joi.string().required(),
      GITHUB_CALLBACK: Joi.string().required(),

      //sendgrid
      SEND_GRID_API_KEY: Joi.string().required(),
    });

    const { error, value: validatedEnvConfig } =
      envVarsSchema.validate(envConfig);

    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }

    const jwtComfirmSecret = String(validatedEnvConfig.JWT_COMFIRM_SECRET);
    const jwtSecret = String(validatedEnvConfig.JWT_SECRET);

    if (jwtComfirmSecret === jwtSecret) {
      throw new Error(
        'Jwt secret key and jwt comfirm secret key must be different',
      );
    }

    return validatedEnvConfig;
  }

  get nodeEnv(): string {
    return String(this.envConfig.NODE_ENV);
  }

  get env(): string {
    return String(this.envConfig.ENV);
  }

  get internalHost(): string {
    return String(this.envConfig.INTERNAL_HOST);
  }

  get internalPort(): number {
    return Number(this.envConfig.INTERNAL_PORT);
  }

  get getJwtComfirmSecret(): string {
    return String(this.envConfig.JWT_COMFIRM_SECRET);
  }

  get jwtSecret(): string {
    return String(this.envConfig.JWT_SECRET);
  }

  get bcryptSalt(): number {
    return Number(this.envConfig.BCRYPT_SALT);
  }

  get feHost(): string {
    return String(this.envConfig.FE_HOST);
  }

  get beHost(): string {
    return String(this.envConfig.BE_HOST);
  }

  get googleClientId(): string {
    return String(this.envConfig.GOOGLE_CLIENT_ID);
  }

  get googleClientSecret(): string {
    return String(this.envConfig.GOOGLE_CLIENT_SECRET);
  }

  get googleCallback(): string {
    return String(this.envConfig.GOOGLE_CALLBACK);
  }

  get gitHubClientId(): string {
    return String(this.envConfig.GITHUB_CLIENT_ID);
  }

  get gitHubClientSecret(): string {
    return String(this.envConfig.GITHUB_CLIENT_SECRET);
  }

  get gitHubCallback(): string {
    return String(this.envConfig.GITHUB_CALLBACK);
  }

  get sendGridApiKey(): string {
    return String(this.envConfig.SEND_GRID_API_KEY);
  }

  get dbURI(): string {
    // tslint:disable-next-line:max-line-length
    return `mongodb+srv://${this.envConfig.DATABASE_USER}:${this.envConfig.DATABASE_PASSWORD}@${this.envConfig.DATABASE_HOST}/${this.envConfig.DATABASE_NAME}?retryWrites=true&w=majority`;
    // tslint:disable-next-line:max-line-length
    // return `mongodb://${this.envConfig.DATABASE_USER}:${this.envConfig.DATABASE_PASSWORD}@${this.envConfig.DATABASE_HOST}:${this.envConfig.DATABASE_PORT}/${this.envConfig.DATABASE_NAME}?authSource=admin`;
  }
}
