import { Module } from '@nestjs/common';
import { SampleModule } from './domains/samples/sample.module';
import { UserModule } from './domains/users/users.module';
import { AuthModule } from './middlewares/auth/auth.module';
import { UserAccessModule } from './domains/userAccesses/userAccesses.module';
import { OtpModule } from './domains/otps/otps.module';

@Module({
  imports: [AuthModule, SampleModule, UserModule, UserAccessModule, OtpModule],
})
export class AppModule {}
